# Scalar ASCII output created by CarpetIOScalar
# created on erik-schnetters-macbook-pro.local by eschnett on Jan 06 2012 at 10:37:31-0500
# parameter filename: "/Users/eschnett/Cvanilla/arrangements/CactusNumerical/RotatingSymmetry180/test/KerrSchild-rotating-180.par"
#
# ADMBASE::alp (admbase::lapse)
# 1:iteration 2:time 3:data
# data columns: 3:alp
0 0 0.880910251369991
1 0.1 0.892358936212168
2 0.2 0.902131550458816
3 0.3 0.910081125912866
