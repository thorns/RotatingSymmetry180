# Scalar ASCII output created by CarpetIOScalar
# created on erik-schnetters-macbook-pro.local by eschnett on Jan 06 2012 at 10:37:33-0500
# parameter filename: "/Users/eschnett/Cvanilla/arrangements/CactusNumerical/RotatingSymmetry180/test/KerrSchild-rotating-180.par"
#
# ML_ADMCONSTRAINTS::M1 (ml_admconstraints::ml_mom)
# 1:iteration 2:time 3:data
# data columns: 3:M1 4:M2 5:M3
0 0 0.843064699531152 0.843064699533143 0.45525135244289
1 0.1 0.696830480706227 0.696830480708939 0.344600325828372
2 0.2 0.567114856610457 0.567114856612553 0.298083452756493
3 0.3 0.512727110881128 0.512727110882713 0.304036531528726
