# Scalar ASCII output created by CarpetIOScalar
# created on erik-schnetters-macbook-pro.local by eschnett on Jan 06 2012 at 10:37:31-0500
# parameter filename: "/Users/eschnett/Cvanilla/arrangements/CactusNumerical/RotatingSymmetry180/test/KerrSchild-rotating-180.par"
#
# ADMBASE::betax (admbase::shift)
# 1:iteration 2:time 3:data
# data columns: 3:betax 4:betay 5:betaz
0 0 0.210175434745621 0.210175434745621 0.211497817135017
1 0.1 0.210115323687716 0.210115323687716 0.211541398734852
2 0.2 0.210011183537544 0.210011183537544 0.211491103960372
3 0.3 0.209882456516174 0.209882456516174 0.211207860262056
