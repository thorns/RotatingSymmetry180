# Scalar ASCII output created by CarpetIOScalar
# created on erik-schnetters-macbook-pro.local by eschnett on Jan 06 2012 at 10:37:32-0500
# parameter filename: "/Users/eschnett/Cvanilla/arrangements/CactusNumerical/RotatingSymmetry180/test/KerrSchild-rotating-180.par"
#
# ADMBASE::betax (admbase::shift)
# 1:iteration 2:time 3:data
# data columns: 3:betax 4:betay 5:betaz
0 0 0.822606011971556 0.519090029488138 0.554798492921223
1 0.1 0.79369972034977 0.516861039576311 0.554082469198382
2 0.2 0.756326827661687 0.521175756638724 0.552138833369612
3 0.3 0.714360259730672 0.529720109019209 0.54909907356138
