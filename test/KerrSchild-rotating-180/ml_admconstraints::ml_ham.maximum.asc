# Scalar ASCII output created by CarpetIOScalar
# created on erik-schnetters-macbook-pro.local by eschnett on Jan 06 2012 at 10:37:33-0500
# parameter filename: "/Users/eschnett/Cvanilla/arrangements/CactusNumerical/RotatingSymmetry180/test/KerrSchild-rotating-180.par"
#
# ML_ADMCONSTRAINTS::H (ml_admconstraints::ml_ham)
# 1:iteration 2:time 3:data
# data columns: 3:H
0 0 101.421759173993
1 0.1 89.1228707854559
2 0.2 75.9323417087396
3 0.3 66.2957545765552
