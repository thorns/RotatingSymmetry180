# Scalar ASCII output created by CarpetIOScalar
# created on erik-schnetters-macbook-pro.local by eschnett on Jan 06 2012 at 10:37:33-0500
# parameter filename: "/Users/eschnett/Cvanilla/arrangements/CactusNumerical/RotatingSymmetry180/test/KerrSchild-rotating-180.par"
#
# ADMBASE::dtbetax (admbase::dtshift)
# 1:iteration 2:time 3:data
# data columns: 3:dtbetax 4:dtbetay 5:dtbetaz
0 0 0 0 0
1 0.1 -1.53724571981997 -1.91436211351678 -0.113098055016485
2 0.2 -1.3392595666112 -0.959633799830775 -0.36562684040996
3 0.3 -0.738112638463448 -0.567574998323015 -0.709145842814749
